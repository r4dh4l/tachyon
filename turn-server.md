# TURN ("Traversal Using Relays around NAT") server

## KB

- https://en.wikipedia.org/wiki/Traversal_Using_Relays_around_NAT

> Traversal Using Relays around NAT (TURN) is a protocol that assists in traversal of network address translators (NAT) or firewalls for multimedia applications. It may be used with the Transmission Control Protocol (TCP) and User Datagram Protocol (UDP). It is most useful for clients on networks masqueraded by symmetric NAT devices. TURN does not aid in running servers on well known ports in the private network through a NAT; it supports the connection of a user behind a NAT to only a single peer, as in telephony, for example.

> TURN is specified by RFC 5766. An update to TURN for IPv6 is specified in RFC 6156. The TURN URI scheme is documented in RFC 7065. 

## coturn

### Installation

```
apt install coturn
```

### Configuration

#### Matrix / Synapse

- https://github.com/matrix-org/synapse/blob/master/docs/turn-howto.rst

Edit the config file in `/etc/turnserver.conf`. The relevant lines for minimum recommendations are:

```
 #listening-port=3478
listening-port=3478
[...]
 #tls-listening-port=5349
tls-listening-port=5349
[...]
 #alt-listening-port=0
alt-listening-port=0
[...]
 #alt-tls-listening-port=0
alt-tls-listening-port=0
[...]
 #listening-ip=yuhu.ddns.net
[...]
 #min-port=49152
 #max-port=65535
 # depending on other services using UDP like Mumble (using 64738) adjust UDP port range
min-port=64000
max-port=64736
[...]
lt-cred-mech
[...]
use-auth-secret
[...]
 # Note by r4dh4l: According to https://github.com/matrix-org/synapse/blob/master/docs/turn-howto.rst "One way to generate the static-auth-secret is with pwgen: pwgen -s 64 1" - according Matrix Synapse homeserver.yaml entry is "turn_shared_secret"
static-auth-secret=YourStaticAuthSecret
[...]
server-name=example.com
[...]
 #userdb=/var/db/turndb
[...]
user-quota=12
[...]
 # Matrix-Synapse comment from https://github.com/matrix-org/synapse/blob/master/docs/turn-howto.rst: VoIP traffic is all UDP. There is no reason to let users connect to arbitrary TCP endpoints via the relay.
 # Should be set if TURN server is part of a private network without public IP address and private network is blocked by "denied-peer-ip" parameter
no-tcp-relay
[...]
 #cert=/usr/local/etc/turn_server_cert.pem
cert=/etc/letsencrypt/live/example.com/fullchain.pem
[...]
 #pkey=/usr/local/etc/turn_server_pkey.pem
pkey=/etc/letsencrypt/live/example.com/privkey.pem
[...]
 #dh-file=<DH-PEM-file-name>
dh-file=/etc/ssl/dhparam.pem
[...]
 #no-stdout-log
no-stdout-log
[...]
 #syslog
syslog
[...]
 # Matrix-Synapse comment from https://github.com/matrix-org/synapse/blob/master/docs/turn-howto.rst: don't let the relay ever try to connect to private IP address ranges within your network (if any); given the turn server is likely behind your firewall, remember to include any privileged public IPs too.
denied-peer-ip=10.0.0.0-10.255.255.255
denied-peer-ip=172.16.0.0-172.31.255.255
denied-peer-ip=192.168.0.0-192.168.255.255
 # Matrix-Synapse comment from https://github.com/matrix-org/synapse/blob/master/docs/turn-howto.rst: special case the turn server itself so that client->TURN->TURN->client flows work
allowed-peer-ip=10.0.0.1
[...]
 #pidfile="/var/run/turnserver.pid"
pidfile="/var/run/turnserver.pid"
[...]
 #no-sslv3
no-sslv3
 #no-tlsv1
no-tlsv1
```

If you don't have `dhparam*.pem` file you can generate it via (attention: needs a while, use a `screen` session!):

```
openssl dhparam -out /etc/ssl/dhparam4096.pem 4096
```

#### Firewall settings for coturn

Open your firewall(s) for this port, when using ufw:

```
ufw status verbose
ufw allow 3478 comment 'TURN listening port for UDP and TCP'
ufw allow 3479 comment 'TURN alt. listening port for UDP and TCP'
ufw allow 5349 comment 'TURN TLS listening port for UDP and TCP'
ufw allow 5350 comment 'TURN alt. TLS listening port for UDP and TCP'
ufw allow 64000:64737/udp comment 'TURN lower and upper bounds of the UDP relay endpoints'
ufw status verbose
```

All commands in one:

```
ufw status verbose && ufw allow 3478 comment 'TURN listening port for UDP and TCP' && ufw allow 3479 comment 'TURN alt. listening port for UDP and TCP' && ufw allow 5349 comment 'TURN TLS listening port for UDP and TCP' && ufw allow 5350 comment 'TURN alt. TLS listening port for UDP and TCP' && ufw allow 64000:64737/udp comment 'TURN lower and upper bounds of the UDP relay endpoints' && ufw status verbose
```

According to `man ufw` these rules without protocol parameter will be opened for TCP *and* UDP:

> ufw supports several different protocols. The following are valid in any rule and enabled when the protocol is not specified:
> tcp
> udp

#### Setup for running behind a dynamic external IP

This concept was developed by Kai Kalua <kai@i2pmail. org>.

- https://matrix.to/#/!KASfJrYmKcbNDbGtnU:yuhu.ddns.net/$153675459533wNZsM:matrix.chat-secure.de

To run a TURN server behind a dynamic IP you need a setup checking the current IP and restarting the TURN server with the valid IP every time it changes. To achieve this you need a setup that does the following:

1. A systemd unit `turnserver_chk_ext_ip_changes_changes.timer`, autostarting, starts another systemd unit `turnserver_chk_ext_ip_changes_changes.service` every 2min.
2. The systemd unit `turnserver_chk_ext_ip_changes_changes.service`, not autostarting, checks the ext. dyn. IP via a script `/usr/local/bin/chk_ext_ip_changes.sh` and comparing the detected IP it with the last checked IP. If the IP changed the new IP will be written in `/var/cache/chk_ext_ip_changes/changed` (line `EXTERNAL_IP_ADDRESS=[NewIP]`).
3. Another systemd unit `turnserver_ext_ip_changed.path`, autostarting, watches `/var/cache/chk_ext_ip_changes/changed`. On changes it executes `turnserver_ext_ip_changed.service`.
4. A systemd unit `turnserver_ext_ip_changed.service`, not autostarting, restarts the TURN server via `turnserver.service` using `/var/cache/chk_ext_ip_changes/changed` as value for the coturn paramter `-X` / `--external-ip` (so coturn is restarted with the changed ext. IP).
5. In case of a server reboot a crontab command removes the files in `/var/cache/chk_ext_ip_changes_changes/` so that `/usr/local/bin/chk_ext_ip_changes.sh` will check the ext. IP against an empty directory `/var/cache/chk_ext_ip_changes_changes/` (otherwise it can happen that the ext. dyn. IP is still the one listed in `/var/cache/chk_ext_ip_changes_changes/` so the turnserver won't be started).

In other words we need:

- 1 systemd unit `turnserver_chk_ext_ip_changes_changes.timer`, autostarting, starting `turnserver_chk_ext_ip_changes_changes.service` every 2min
- 1 systemd unit `turnserver_chk_ext_ip_changes_changes.service`, not autostarting, checking the ext. dyn. IP via a script `/usr/local/bin/chk_ext_ip_changes.sh` and comparing it with the last checked IP, on IP change new IP will be written in `/var/cache/chk_ext_ip_changes/changed` (line `EXTERNAL_IP_ADDRESS=[NewIP]`)
- 1 script `/usr/local/bin/chk_ext_ip_changes.sh` which is used to check the ext. dyn. IP
- 1 systemd unit `turnserver_ext_ip_changed.path`, autostarting, watching `/var/cache/chk_ext_ip_changes/changed` and executing `turnserver_ext_ip_changed.service` if something changed in `/var/cache/chk_ext_ip_changes/changed`
- 1 systemd unit `turnserver_ext_ip_changed.service`, not autostarting, restarting the TURN server via `/usr/bin/systemctl restart turnserver.service` using `/var/cache/chk_ext_ip_changes/changed` as value for coturn paramter `-X` / `--external-ip` (so coturn is restarted with the changed ext. IP)
- 1 systemd unit `turnserver.service`, not autostarting (because a valid IP is needed), using `/var/cache/chk_ext_ip_changes/changed` as environment file
- 1 crontab entry removing the content in `/var/cache/chk_ext_ip_changes_changes/` after a server reboot, for example: `@reboot sleep 40 && rm /var/cache/chk_ext_ip_changes_changes/*`

The systemd version (`232-25+deb9u4`) of the Debian9 server this setup was built for has certain limitations related to this setup:

1. The coturn PID file had to be relocated from `/var/run/turnserver.pid` to `/var/tmp/turnserver.pid`.
2. A part of `/usr/local/bin/chk_ext_ip_changes.sh` has to check if `/var/cache/chk_ext_ip_changes/` exists and create it if necessary. Therefor the script has to be executed as `root`.

To specify a cache, state or runtime directory a systemd version >= 235 is required.

##### Script and systemd unit preparation

```
ls -la /etc/systemd/system/ && ls -la /etc/systemd/system/timers.target.wants/
touch /etc/systemd/system/turnserver_chk_ext_ip_changes_changes.timer
touch /etc/systemd/system/turnserver_chk_ext_ip_changes_changes.service
touch /usr/local/bin/chk_ext_ip_changes.sh
touch /etc/systemd/system/turnserver_ext_ip_changed.path
touch /etc/systemd/system/turnserver_ext_ip_changed.service
ls -la /etc/systemd/system/ && cd /etc/systemd/system/
```

All commands in one:

```
ls -la /etc/systemd/system/ && ls -la /etc/systemd/system/timers.target.wants/ && touch /etc/systemd/system/turnserver_chk_ext_ip_changes_changes.timer && touch /etc/systemd/system/turnserver_chk_ext_ip_changes_changes.service && touch /usr/local/bin/chk_ext_ip_changes.sh && touch /etc/systemd/system/turnserver_ext_ip_changed.path && touch /etc/systemd/system/turnserver_ext_ip_changed.service && ls -la /etc/systemd/system/ && ls -la /etc/systemd/system/timers.target.wants/ && cd /etc/systemd/system/
```

###### turnserver_chk_ext_ip_changes_changes.timer

Into `/etc/systemd/system/turnserver_chk_ext_ip_changes.timer` paste:

```
[Unit]
Description=Executes turnserver_chk_ext_ip_changes_changes.service every two minutes
Wants=network-online.target
After=network-online.target

[Timer]
OnBootSec=2min
OnCalendar=*:0/2

[Install]
 #WantedBy=multi-user.target
WantedBy=timers.target
```

###### turnserver_chk_ext_ip_changes_changes.service

Into `/etc/systemd/system/timers.target.wants/turnserver_chk_ext_ip_changes.timer` paste:

```
[Unit]
Description=Checks if external ip address has changed via chk_ext_ip_changes.sh triggered by turnserver_chk_ext_ip_changes_changes.timer

[Service]
Type=oneshot
 # To specify a state or cache directory as well as the modes
 # systemd >= 235 is required
 #StateDirectory=chk_ext_ip_changes
 #StateDirectoryMode=750
 #CacheDirectory=chk_ext_ip_changes
 #CacheDirectoryMode=750

ExecStart=/usr/local/bin/chk_ext_ip_changes.sh -w
```

###### chk_ext_ip_changes.sh

Into `/usr/local/bin/chk_ext_ip_changes.sh` paste content of `scripts/chk_ext_ip_changes.sh`.

###### turnserver_ext_ip_changed.path

Into `/etc/systemd/system/turnserver_ext_ip_changed.path` paste:

```
[Unit]
Description=Triggers turnserver_ext_ip_changed.service if the external ip address has changed
Documentation= man:systemd.path
Requires=turnserver_chk_ext_ip_changes_changes.timer
After=turnserver_chk_ext_ip_changes_changes.timer

[Path]
PathModified=/var/cache/chk_ext_ip_changes/changed

[Install]
WantedBy=multi-user.target
```

###### turnserver_ext_ip_changed.service

Into `/etc/systemd/system/turnserver_ext_ip_changed.service` paste:

```
[Unit]
Description=Restarts coturn via turnserver.service triggered by turnserver_ext_ip_changed.path
ConditionPathExists=/var/cache/chk_ext_ip_changes/changed

[Service]
Type=oneshot
ExecStart=/bin/systemctl restart turnserver.service

[Install]
WantedBy=multi-user.target
```

###### turnserver.service

See section "Working systemd unit for coturn 4.5.0.5-1+b1".

##### Enable setup

```
chmod u+x /usr/local/bin/chk_ext_ip_changes.sh && ls -la /usr/local/bin/chk_ext_ip_changes.sh && /usr/local/bin/chk_ext_ip_changes.sh
ln -s /etc/systemd/system/turnserver_chk_ext_ip_changes_changes.timer /etc/systemd/system/timers.target.wants/turnserver_chk_ext_ip_changes_changes.timer && ls -la /etc/systemd/system/timers.target.wants/
systemctl daemon-reload
systemctl enable turnserver_chk_ext_ip_changes_changes.timer
systemctl enable turnserver_ext_ip_changed.path
systemctl start turnserver_chk_ext_ip_changes_changes.timer
systemctl start turnserver_ext_ip_changed.path
systemctl status turnserver_chk_ext_ip_changes_changes.timer
systemctl status turnserver_ext_ip_changed.path
systemctl list-unit-files | grep turn
ls -la /var/cache/chk_ext_ip_changes/
```

All commands in one:

```
chmod u+x /usr/local/bin/chk_ext_ip_changes.sh && ls -la /usr/local/bin/chk_ext_ip_changes.sh && /usr/local/bin/chk_ext_ip_changes.sh && ln -s /etc/systemd/system/turnserver_chk_ext_ip_changes_changes.timer /etc/systemd/system/timers.target.wants/turnserver_chk_ext_ip_changes_changes.timer && ls -la /etc/systemd/system/timers.target.wants/ && systemctl daemon-reload && systemctl enable turnserver_chk_ext_ip_changes_changes.timer && systemctl enable turnserver_ext_ip_changed.path && systemctl start turnserver_chk_ext_ip_changes_changes.timer && systemctl start turnserver_ext_ip_changed.path && systemctl status turnserver_chk_ext_ip_changes_changes.timer && systemctl status turnserver_ext_ip_changed.path && systemctl list-unit-files | grep turnserver && ls -la /var/cache/chk_ext_ip_changes/
```

`turnserver.service` must **not** autostart because it needs a valid IP as parameter. If the system reboots `chk_ext_ip_changes.sh` will take care to start `turnserver.service`.

Wait two minutes and check if turnserver was started by the setup:

```
systemctl status turnserver.service && ls -la /var/cache/chk_ext_ip_changes/
```

##### Crontab entry for reboot case

Under `root` add in `crontab -e`:

```
@reboot sleep 40 && rm /var/cache/chk_ext_ip_changes_changes/*
```

Otherwise after a sudden reboot the ext. IP and the IPs in `/var/cache/chk_ext_ip_changes_changes/` are the same so the turnserver won't be started.

##### Debuggin setup

In `/etc/turnserver.conf` activate

- verbose
- log-file=/var/log/turnserver/turn.log

After

```
systemctl restart turnserver.service && systemctl -l --no-pager status turnserver.service
```

try to call again and you will find call logs in `/var/log/turnserver/turnserver-YYYY-MM-DD.log`.

#### Working systemd unit for coturn 4.5.0.5-1+b1

The init script for Debian coturn version `4.5.0.5-1+b1` seems to have a bug because it does not start coturn neither on `service coturn start` nor `systemctl start coturn.service`.

As workaround you can set up a custom one:

```
touch /etc/systemd/system/turnserver.service
```

The name `turnserver.service` should be used to avoid interferences with the existing coturn init script.

In `/etc/systemd/system/turnserver.service` paste:

```
[Unit]
Description=TURN server coturn (custom systemd script)
Documentation=man:coturn(1) man:turnadmin(1) man:turnserver(1)
After=syslog.target network.target

[Service]
User=turnserver
Group=turnserver
Type=forking
EnvironmentFile=/etc/default/coturn
EnvironmentFile=/var/cache/chk_ext_ip_changes/changed

 # See comment below (RuntimeDirectory)
 #PIDFile=/var/run/turnserver/turnserver.pid
PIDFile=/var/tmp/turnserver.pid

 # 1. Using "coturn" will conflict with the SysInitV script of coturn.
 #    This is the reason, why the service unit is called "turnserver.service", too
 # 2. To specify a state or cache directory as well as the modes
 #    systemd >= 235 is required, this system uses 232 :-(
 #RuntimeDirectory=turnserver
 #RuntimeDirectoryMode=0750

 # See comment above (RuntimeDirectory)
 #ExecStart=/usr/bin/turnserver --daemon --pidfile /var/run/turnserver/turnserver.pid -c /etc/turnserver.conf --external-ip $EXTERNAL_IP_ADDRESS $EXTRA_OPTIONS
ExecStart=/usr/bin/turnserver --daemon --pidfile /var/tmp/turnserver.pid -c /etc/turnserver.conf --external-ip $EXTERNAL_IP_ADDRESS $EXTRA_OPTIONS

 # See comment above (RuntimeDirectory)
 #ExecStopPost=/bin/rm -f /var/run/turnserver/turnserver.pid
ExecStopPost=/bin/rm -f /var/tmp/turnserver.pid
Restart=on-abort

LimitCORE=infinity
LimitNOFILE=999999
LimitNPROC=60000
LimitRTPRIO=infinity
LimitRTTIME=7000000
CPUSchedulingPolicy=other
UMask=0007

[Install]
WantedBy=multi-user.target
```

```
update-rc.d coturn disable && service coturn stop && service coturn status

systemctl service-reload && systemctl enable turnserver.service && systemctl start turnserver.service &&systemctl status turnserver.service
```

### Operation

#### Start

Installed via APT:

```
service coturn start && service coturn status
```

Attention: In case of Debian stretch coturn version `4.5.0.5-1+b1` the init script seems not starting the server. In this case you have to use the script described in "Working systemd unit for coturn 4.5.0.5-1+b1".

Built version:

```
bin/turnserver -o
```

### Common problems

#### "check_stun_auth: Cannot find credentials of user"

- https://github.com/coturn/coturn/issues/180

> the solution was to remove shared secrets from the config and turnadmin. Once I did this things worked.

#### "Media Connection Failed"

- https://github.com/vector-im/riot-android/issues/1744#issuecomment-364596648

See "Setup for running behind a dynamic external IP".
