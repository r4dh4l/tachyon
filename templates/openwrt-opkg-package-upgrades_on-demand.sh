#!/bin/ash
#
# This script will upgrade the OPKG packages of your OpenWRT system on demand executed via SSH remotely.
# It is an adjusted version of "openwrt-update.sh" 2018 v0.1 by Kai Kalua <kai@i2pmail. org>
#
# Installation
# ------------
# To enable status notifications via mail activate the deactivated "exec mailsend" commands in this script and install mailsend on your router:
#  1. ssh root@[OpenWRT Router] 'opkg install mailsend'
# If you don't need status notifications via mail just go on with:
#  2. ssh root@[OpenWRT Router] 'mkdir -p /usr/local/sbin'
#  3. cat ./openwrt-opkg-package-upgrades_on-demand.sh | ssh root@[OpenWRT Router] 'cat -> /usr/local/sbin/openwrt-opkg-package-upgrades_on-demand.sh'
#  4. ssh root@[OpenWRT Router] 'chmod 700 /usr/local/sbin/openwrt-opkg-package-upgrades_on-demand.sh'
# To add the script to manual backup procedure:
#  5. ssh root@[OpenWRT Router] 'echo "/usr/local/sbin/" >> /etc/sysupgrade.conf'
#  6. ssh root@[OpenWRT Router] 'echo "/www/update.log" >> /etc/sysupgrade.conf'
#
# Usage / Operation
# -----
#  ssh root@[OpenWRT Router] '/usr/local/sbin/openwrt-opkg-package-upgrades_on-demand.sh'
#
# Check the results
# ------------------
#  Open https://[OpenWRT Router]/openwrt-opkg-package-upgrades_on-demand_log.txt in your browser.
#
# (c) 2018 Kai Kalua <kai@i2pmail. org>
# (c) 2020 r4dh4l <r4dh4l@posteo. net>
#
# License: GPL v2 (https://www.gnu.org/licenses/old-licenses/gpl-2.0.de.html)
#
#          You're allowed to publish the code if you follow every other rule
#          of the GPL v2
#
# Version: 0.1
#

LOG_FILE="/www/openwrt-opkg-package-upgrades_on-demand_log.txt"
DATE_PARAM="+%Y-%m-%d_%H-%M-%S"

# Update the OPKG package list
opkg update || exit 52

# Upgrade "netifd" because it otherwise breaks the upgrade process if it will be upgraded next to other packages
opkg upgrade netifd

# Create a list of packages OPKG can upgrade
PACKAGES="$(opkg list-upgradable |awk '{print $1}')"

# Upgrade the packages
if [ -n "${PACKAGES}" ]; then
    # Just an echo to clear the log file
    echo "$(date $DATE_PARAM) - Dry run for package upgrades" > "$LOG_FILE"
    # Dry run
    opkg --noaction upgrade ${PACKAGES}
    if [ "$?" -eq 0 ]; then
    echo "$(date $DATE_PARAM) - Dry run successful" >> "$LOG_FILE"
    # Real upgrade
    echo "$(date $DATE_PARAM) - Starting real package upgrades" >> "$LOG_FILE"
    opkg upgrade ${PACKAGES}
    RC=$?
    if [ "$RC" -eq 0 ]; then
        echo "$(date $DATE_PARAM) - Package upgrades successful" >> "$LOG_FILE"
        #echo "$(date $DATE_PARAM) - Rebooting OpenWRT" >> "$LOG_FILE"
        #exec mailsend -smtp SmtpServerOfYourProvider -port 465/587 -t MailAddressToReceive -f MailAddressToSend -sub 'openwrt-opkg-package-upgrades_on-demand.sh status' -ssl/tls/starttls -auth -user MailAccountLogin -pass "MailAccountLoginPassword" -mime-type "text/plain" -msg-body "openwrt-opkg-package-upgrades_on-demand_log.txt" 
        #exec reboot
    else
        echo "$(date $DATE_PARAM) - Package upgrades failed. Return code: $RC" >> "$LOG_FILE"
        #exec mailsend -smtp SmtpServerOfYourProvider -port 465/587 -t MailAddressToReceive -f MailAddressToSend -sub 'openwrt-opkg-package-upgrades_on-demand.sh status' -ssl/tls/starttls -auth -user MailAccountLogin -pass "MailAccountLoginPassword" -mime-type "text/plain" -msg-body "openwrt-opkg-package-upgrades_on-demand_log.txt" 
    fi
    else
    echo "$(date $DATE_PARAM) - Dry run failed. Packages will not be upgraded!" >> "$LOG_FILE"
    #exec mailsend -smtp SmtpServerOfYourProvider -port 465/587 -t MailAddressToReceive -f MailAddressToSend -sub 'openwrt-opkg-package-upgrades_on-demand.sh status' -ssl/tls/starttls -auth -user MailAccountLogin -pass "MailAccountLoginPassword" -mime-type "text/plain" -msg-body "openwrt-opkg-package-upgrades_on-demand_log.txt" 
    fi
else
    echo "$(date $DATE_PARAM) - No package upgrades available" > "$LOG_FILE"
    #exec mailsend -smtp SmtpServerOfYourProvider -port 465/587 -t MailAddressToReceive -f MailAddressToSend -sub 'openwrt-opkg-package-upgrades_on-demand.sh status' -ssl/tls/starttls -auth -user MailAccountLogin -pass "MailAccountLoginPassword" -mime-type "text/plain" -msg-body "openwrt-opkg-package-upgrades_on-demand_log.txt" 
fi
